//
//  NSURL.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import Foundation

extension NSURL {
    
    func parseQueryString() -> [String : String] {
        if let query = self.query {
            let components = query.componentsSeparatedByString("&").map({ (components: String) -> (String, String) in
                let kv = components.componentsSeparatedByString("=")
                switch kv.count {
                case 0:
                    return ("", "")
                case 1:
                    return (kv[0], "")
                case 2:
                    return (kv[0], kv[1])
                default:
                    return ("", "")
                }
            })
            
            var dict = [String : String]()
            for component in components {
                dict[component.0.stringByRemovingPercentEncoding ?? ""] = component.1.stringByRemovingPercentEncoding ?? ""
            }
            return dict
            
        } else {
            return [:]
        }
    }
    
    var isHostedByMine: Bool {
        var res = false
        if let host0 = NSURL(string: Environment.URL.WEB)?.host, host1 = host {
            res = host0 == host1
        } else if let host0 = NSURL(string: Environment.URL.API)?.host, host1 = host {
            res = host0 == host1
        }
        
        return res
    }
    
    var isHostedByOther: Bool {
        return !isHostedByMine
    }
}