//
//  String.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import UIKit

extension String {
    
    var trim: String {
        let set = NSCharacterSet(charactersInString: " 　")
        return self.stringByTrimmingCharactersInSet(set)
    }
    
    var nonEmpty: Bool {
        return !self.isEmpty
    }
    
    func URLStringByAppendingQueryString(queryString: String) -> String {
        if queryString.isEmpty {
            return self
        }
        
        let splitter = (self as NSString).rangeOfString("?").length > 0 ? "&" : "?"
        return "\(self)\(splitter)\(queryString)"
    }
    
    func stringByAppendingPathComponent(str: String) -> NSString {
        return (self as NSString).stringByAppendingPathComponent(str) as NSString
    }
    
    func heightWithFont(font: UIFont, width: CGFloat) -> CGFloat {
        return (self as NSString).boundingRectWithSize(
            CGSizeMake(width, CGFloat.max),
            options: NSStringDrawingOptions.UsesLineFragmentOrigin,
            attributes: [NSFontAttributeName: font],
            context: nil
        ).height
    }
    
    func heightWithFont(font: UIFont, width: CGFloat, numberOfLines: Int) -> CGFloat {
        let oneLineHeight = ("あ" as NSString).boundingRectWithSize(
            CGSizeMake(CGFloat.max, CGFloat.max),
            options: NSStringDrawingOptions.UsesLineFragmentOrigin,
            attributes: [NSFontAttributeName: font],
            context: nil
        ).height
        
        let boundingHeight = heightWithFont(font, width: width)
        
        if Int(boundingHeight / oneLineHeight) >= numberOfLines {
            return oneLineHeight * CGFloat(numberOfLines)
        } else {
            return boundingHeight
        }
    }
}