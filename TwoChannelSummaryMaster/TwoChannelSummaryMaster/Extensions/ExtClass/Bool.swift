//
//  Bool.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import Foundation

extension Bool {
    func toInt() -> Int {
        return self ? 1 : 0
    }
}

extension Int {
    func toBool() -> Bool {
        return self == 0 ? false : true
    }
}