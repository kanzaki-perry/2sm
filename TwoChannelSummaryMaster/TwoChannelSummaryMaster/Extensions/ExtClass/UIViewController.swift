//
//  UIViewController.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import UIKit

extension UIViewController {
    var backButtonTitle : String? {
        get {
            if let backBarButtonItem = navigationItem.backBarButtonItem {
                return backBarButtonItem.title
            } else {
                return nil
            }
        }
        set {
            let backBarButtonItem = UIBarButtonItem()
            backBarButtonItem.title = newValue
            navigationItem.backBarButtonItem = backBarButtonItem
            navigationItem.backBarButtonItem?.enabled = true
        }
    }
}
