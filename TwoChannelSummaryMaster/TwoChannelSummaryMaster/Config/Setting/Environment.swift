//
//  Environment.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import Foundation

//
// 環境設定切替
// APIおよびWebの接続先を変更する場合は、以下のタイプエイリアスを切り替えて制御する
#if DEBUG
typealias Environment = Dev
#else
typealias Environment = Dev
#endif

//
// 接続先環境別設定定義
// 接続先によって異なる定義については、以下に定義する
struct Local {
    struct URL {
//        static let API  = "http://10.199.80.147:3000"
//        static let WEB  = "http://10.199.80.147:3000"
        
        static let API  = "http://localhost:3000"
        static let WEB  = "http://localhost:3000"
    }
}

struct Dev {
    struct URL {
        static let API  = "http://blueperry.com/2csmserver/api/"
        static let WEB  = "http://blueperry.com/2csmserver/"
    }
}

//Todo: 本番サーバー立ち上げたらリリース前に必ず書き換える！！！！
struct Prod {
    struct URL {
        static let API  = "http://blueperry.com/2csmserver/api/"
        static let WEB  = "http://blueperry.com/2csmserver/"
    }
}
