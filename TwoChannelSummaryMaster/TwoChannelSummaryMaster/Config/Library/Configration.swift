//
//  Configration.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import Foundation
import SecureNSUserDefaults

typealias Conf = Configration

class Configration: NSObject {
    
    enum Keys: String {
        case DeviceToken
        case DeviceId
        
        /*
        case ApnsPermissionDialogDisplayed
        case ApnsPermissionRequested
        case ApnsDeviceTokenRegistered
         */
    }
    
    let ud = NSUserDefaults.standardUserDefaults()
    
    static let sharedInstance = Configration()
    
    private override init() {
        super.init()
        NSUserDefaults.standardUserDefaults().setSecret("TwoChunnelSummaryMasterUD")
    }
    
    var deviceToken: String? {
        get { return ud.secretStringForKey(Keys.DeviceToken.rawValue) }
        set { sync { self.ud.setSecretObject(newValue, forKey: Keys.DeviceToken.rawValue) } }
    }
    
    var deviceId: String? {
        get { return ud.secretStringForKey(Keys.DeviceId.rawValue) }
        set { sync { self.ud.setSecretObject(newValue, forKey: Keys.DeviceId.rawValue) } }
    }
    
    /* Todo: プッシュ通知本格実装時に使う予定
    var isApnsPermissionDialogDisplayed: Bool {
        get { return ud.secretBoolForKey(Keys.ApnsPermissionDialogDisplayed.rawValue) }
        set { sync { self.ud.setSecretBool(newValue, forKey: Keys.ApnsPermissionDialogDisplayed.rawValue) } }
    }
    
    var isApnsDeviceTokenRegistered: Bool {
        get { return ud.secretBoolForKey(Keys.ApnsDeviceTokenRegistered.rawValue) }
        set { sync { self.ud.setSecretBool(newValue, forKey: Keys.ApnsDeviceTokenRegistered.rawValue) } }
    }
    
    var isApnsPermissionRequested: Bool {
        get { return ud.secretBoolForKey(Keys.ApnsPermissionRequested.rawValue) }
        set { sync { self.ud.setSecretBool(newValue, forKey: Keys.ApnsPermissionRequested.rawValue) } }
    }
    */
    
    func sync(f: () -> Void) -> Bool {
        f()
        return ud.synchronize()
    }
}