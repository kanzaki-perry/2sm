//
//  SystemInfo.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import Foundation
import UIKit

struct SystemInfo {
    
    struct Device {
        
        // 端末名(iPhone, iPad, etc...)
        static var Model: String {
            return UIDevice.currentDevice().model
        }
    }
    
    struct OS {
        
        // OS名(iOS, etc...)
        static var Name: String {
            return UIDevice.currentDevice().systemName
        }
        
        // OSバージョン(7.0, 7.1, 8.0, etc...)
        static var Version: String {
            return UIDevice.currentDevice().systemVersion
        }
        
        // 言語設定(ja, en, etc...)
        static var Language: String {
            let v = NSLocale.currentLocale().objectForKey(NSLocaleLanguageCode) as! String?
            return v ?? "ja"
        }
        
        // ロケール設定(ja_JP, en_US, etc...)
        static var Locale: String {
            let v = NSLocale.currentLocale().objectForKey(NSLocaleIdentifier) as! String?
            return v ?? "ja_JP"
        }
        
        // タイムゾーン
        static var TimeZone: String {
            return NSTimeZone.systemTimeZone().name
        }
        
        // 時差
        static var TimeZoneDifference: String {
            return String(NSTimeZone.systemTimeZone().secondsFromGMT)
        }
    }
    
    struct App {
        
        // アプリバージョン
        static var Version: String {
            let v = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString") as! String?
            return v ?? "1.0.0"
        }
        
        // ビルドバージョン
        static var Build: String {
            let v = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleVersion") as! String?
            return v ?? "1000"
        }
        
        static var UserAgent: String {
            //デフォルトのUserAgentを取得するために、サイズゼロのUIWebViewのインスタンスを生成
            let webView = UIWebView(frame: CGRectZero)
            
            //デフォルトのUserAgentを取得
            let userAgent = webView.stringByEvaluatingJavaScriptFromString("navigator.userAgent") ?? ""
            
            let appendString = "TwoChunnelSummaryMaster iOS"
            
            //デフォルトのUserAgentに独自の文字列を追加
            return userAgent.stringByReplacingOccurrencesOfString(appendString, withString: "", options: [], range: nil).stringByAppendingString(appendString)
        }
    }
}
