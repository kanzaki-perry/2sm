//
//  ColorSet.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import UIKit

//Todo: おおまかな配色傾向が決まり次第使用頻度の高い色を詰め込む
class ColorSet:NSObject{
    static var NavigationBarBackground = UIColor(red: 0.38, green: 0.41, blue: 0.55, alpha: 1)
    static var NewsVoteGood = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
    static var NewsVoteBad = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
    static var TextLerge = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
    static var TextMiddle = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
    static var TextSmall = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
}