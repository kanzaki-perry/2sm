//
//  AppDelegate.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import UIKit
import SecureNSUserDefaults
import SVProgressHUD
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        //初期セットアップ
        setup(application, launchOptions: launchOptions)
        return true
    }
    
    func applicationWillResignActive(application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // バージョンチェック
        AppUtil.checkApplicationVersion(AppUtil.getTopViewController())
    }
    
    func applicationWillTerminate(application: UIApplication) {
        
    }
    
    func application(application: UIApplication,openURL url: NSURL,sourceApplication: String?,annotation: AnyObject) -> Bool {
        return true
    }
    
    func setup(application: UIApplication, launchOptions: [NSObject: AnyObject]?){
        //基本配色を設定
        setAppearance()
        
        setConfig()
        
        addUser()
        
        FIRApp.configure()
        
        //WebView用UserAgent設定
        AppUtil.setWebViewAgent()
    }
    
    //基本配色設定
    func setAppearance(){
        UINavigationBar.appearance().barTintColor = UIColor.blackColor().colorWithAlphaComponent(0.75)
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.yellowColor()]
    }
    
    func setConfig(){
        let config = Configration.sharedInstance
        if(config.deviceId ?? "" == ""){
            config.deviceId = NSUUID().UUIDString
        }
    }
    
    //非同期JSON通信でAPIから一覧データ取得
    func addUser() {
        let appId = Configration.sharedInstance.deviceId ?? ""
        let encodeId = appId.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.alphanumericCharacterSet()) ?? ""
        let request = TCSMWS.Endpoint.AddUser(deviceId: encodeId)
        let future = TCSMWS.invoke(request)
        
        future.onSuccess { res in

        }
        
        future.onFailure { err in

        }
    }
}