//
//  TCSMWSRequestFilter.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import Foundation

class TCSMWSRequestFilter: WSRequestFilter {
    
    func process(request: NSMutableURLRequest) -> NSMutableURLRequest {
        
        // Content-Type
        request.setValue("application/json", forHTTPHeaderField: TCSMWS.Header.ContentType.rawValue)
        
        // OS名
        request.setValue(SystemInfo.OS.Name, forHTTPHeaderField: TCSMWS.Header.OS.rawValue)
        
        // OSバージョン
        request.setValue(SystemInfo.OS.Version, forHTTPHeaderField: TCSMWS.Header.OSVersion.rawValue)
        
        // アプリケーションバージョン
        request.setValue(SystemInfo.App.Version, forHTTPHeaderField: TCSMWS.Header.Version.rawValue)
        
        // ビルドバージョン
        request.setValue(SystemInfo.App.Build, forHTTPHeaderField: TCSMWS.Header.BuildVersion.rawValue)
        
        // デバイス名
        request.setValue(SystemInfo.Device.Model, forHTTPHeaderField: TCSMWS.Header.Device.rawValue)
        
        // ロケール
        request.setValue(SystemInfo.OS.Locale, forHTTPHeaderField: TCSMWS.Header.Locale.rawValue)
        
        // 言語
        request.setValue(SystemInfo.OS.Language, forHTTPHeaderField: TCSMWS.Header.Lang.rawValue)
        
        // タイムゾーン
        request.setValue(SystemInfo.OS.TimeZone, forHTTPHeaderField: TCSMWS.Header.TimeZone.rawValue)
        
        // GMTとの時差
        request.setValue(SystemInfo.OS.TimeZoneDifference, forHTTPHeaderField: TCSMWS.Header.TimeZoneSecondsFromGMT.rawValue)
        
        // ユーザーエージェント
        request.setValue(SystemInfo.App.UserAgent, forHTTPHeaderField: TCSMWS.Header.UserAgent.rawValue)
                
        return request
    }
}
