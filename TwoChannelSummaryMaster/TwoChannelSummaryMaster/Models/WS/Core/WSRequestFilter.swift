//
//  WSRequestFilter.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import Foundation

protocol WSRequestFilter {
    
    func process(request: NSMutableURLRequest) -> NSMutableURLRequest
}