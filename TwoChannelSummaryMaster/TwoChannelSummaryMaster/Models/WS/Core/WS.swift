//
//  WS.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import Foundation

typealias Parameters = [String: AnyObject]

class WS {
    typealias RequestFilters = [WSRequestFilter]
    
    class func baseURL() -> NSURL {
        fatalError("WS.baseURL() must be overrided in subclasses.")
    }
    
    class func filters() -> RequestFilters {
        return []
    }
    
    class func invoke<T: WSRequest>(r: T) -> Future<T.WSResponse, NSError> {
        let promise = Promise<T.WSResponse, NSError>()
        
        request(r).validate(statusCode: 200..<300).response { (req: NSURLRequest?, res: NSHTTPURLResponse?, data: NSData?, error: ErrorType?) -> Void in
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
                if let error = error {
                    promise.failure(NSError(
                        error: error as NSError,
                        request: req,
                        response: res,
                        body: data
                    ))
                    return
                }
                
                if let object = data.flatMap({ r.responseFromObject($0, headers: (res?.allHeaderFields)!) }) {
                    promise.success(object)
                } else {
                    promise.failure(NSError(
                        domain: WSErrorDomain,
                        code: WSError.WSResponseMappedError.rawValue,
                        userInfo: nil,
                        request: req,
                        response: res,
                        body: data
                    ))
                }
            })
        }
        
        return promise.future
    }
    
    class func URLRequest(method: Method, path: String, parameters: Parameters, encoding: ParameterEncoding) -> NSMutableURLRequest {
        let URLRequest = NSMutableURLRequest(URL: baseURL().URLByAppendingPathComponent(path))
        URLRequest.HTTPMethod = method.rawValue
        
        for filter in filters() {
            filter.process(URLRequest)
        }
        
        return  encoding.encode(URLRequest, parameters: parameters).0
    }
}

