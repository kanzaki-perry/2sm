//
//  Optional.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import Foundation

extension Optional {
    
    func filter(f: (Wrapped) -> Bool) -> Wrapped? {
        if let v = self {
            return f(v) ? .Some(v) : .None
        } else {
            return .None
        }
    }
}
