//
//  WSError.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import Foundation
import SwiftyJSON

let WSErrorDomain   = "jp.co.pf.2csm.ws.error"
let WSErrorRequest  = "jp.co.pf.2csm.ws.error.request"
let WSErrorResponse = "jp.co.pf.2csm.ws.error.response"
let WSErrorBody     = "jp.co.pf.2csm.ws.error.body"

enum WSError: Int {
    case WSResponseMappedError = -999
}

extension NSError {

    convenience init(error: NSError, request: NSURLRequest?, response: NSHTTPURLResponse?, body: NSData?) {
        self.init(domain: error.domain, code: error.code, userInfo: error.userInfo, request: request, response: response, body: body)
    }
    
    convenience init(domain: String, code: Int, userInfo: [NSObject : AnyObject]?, request: NSURLRequest?, response: NSHTTPURLResponse?, body: NSData?) {
        
        var uInfo = userInfo ?? [:]
        
        if let request = request {
            uInfo[WSErrorRequest] = request
        }
        
        if let response = response {
            uInfo[WSErrorResponse] = response
        }
        
        if let body = body {
            uInfo[WSErrorBody] = body
        }
        
        self.init(domain: domain, code: code, userInfo: uInfo)
    }
    
    var request: NSURLRequest? {
        return userInfo[WSErrorRequest] as? NSURLRequest
    }
    
    var response: NSHTTPURLResponse? {
        return userInfo[WSErrorResponse] as? NSHTTPURLResponse
    }
    
    var body: NSData? {
        return userInfo[WSErrorBody] as? NSData
    }
    
    var json: JSON {
        let data = body ?? NSData()
        return JSON(data: data, options: [], error: nil)
    }
}