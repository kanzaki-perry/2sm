//
//  WSRequest.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import Foundation

protocol WSRequest: URLRequestConvertible {
    typealias WSResponse: Any
    
    var method: Method { get }
    var path: String { get }
    var parameters: Parameters { get }
    var encoding: ParameterEncoding { get }
    
    func responseFromObject(object: NSData, headers: [NSObject:AnyObject]) -> WSResponse?
}
