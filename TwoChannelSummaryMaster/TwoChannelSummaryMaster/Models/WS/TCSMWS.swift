//
//  TCSMWS.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import Foundation
import SwiftyJSON

let TCSMWSMaintenanceNotificationKey  = "jp.co.pf.2csm.ws.maintenance.notification"

class TCSMWS: WS {
    
    private static var _filters = RequestFilters()
    
    enum Header: String {
        case ContentType            = "Content-Type"
        case OS                     = "X-2csm-OS"
        case OSVersion              = "X-2csm-OSVersion"
        case Version                = "X-2csm-Version"
        case BuildVersion           = "X-2csm-BuildVersion"
        case Device                 = "X-2csm-Device"
        case Locale                 = "X-2csm-Locale"
        case Lang                   = "X-2csm-Lang"
        case TimeZone               = "X-2csm-Timezone"
        case TimeZoneSecondsFromGMT = "X-2csm-TimezoneSecondsFromGMT"
        case SmartDeviceUUID        = "X-2csm-SmartDeviceUUID"
        case DeviceToken            = "X-2csm-DeviceToken"
        case UserAgent              = "User-Agent"
    }
    
    override class func baseURL() -> NSURL {
        return NSURL(string: Environment.URL.API)!
    }
    
    override class func filters() -> RequestFilters {
        return _filters
    }
    
    override class func invoke<T: WSRequest>(r: T) -> Future<T.WSResponse, NSError> {
        let future = super.invoke(r)
        
        future.onFailure { err in
            // メンテナンス時のハンドリング
            if let response = err.response {
                if 503 == response.statusCode {
                    let notification = NSNotification(name: TCSMWSMaintenanceNotificationKey, object: nil)
                    NSNotificationCenter.defaultCenter().postNotification(notification)
                }
            }
        }
        
        return future
    }
    
    class func setDefaultRequestFilters(filters: RequestFilters) {
        _filters = filters
    }
    
    class Endpoint {
        // 各APIエンドポイントの定義は、各APIリクエストクラスで定義すること！
    }
}

class TCSMWSMapper: Mappable {
    
    required init() {}
    
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {}
}

class TCSMWSResult: Mappable {
    init() {}
    
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
    }
}

extension Mapper {
    
    func map(data: NSData) -> N! {
        
    print(JSON(data: data, options: [], error: nil))
        
        if let string = NSString(data: data, encoding: NSUTF8StringEncoding) {
            if (string as String).isEmpty {
                return self.map("{}")
            } else {
                return self.map(string as String)
            }
        }
        return nil
    }
}
