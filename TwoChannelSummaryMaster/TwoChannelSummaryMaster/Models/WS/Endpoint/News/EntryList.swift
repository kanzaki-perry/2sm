//
//  NewsList.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import Foundation

extension TCSMWS.Endpoint {
    typealias EntryList = EntryList$
}

class EntryList$: WSRequest {
    
    typealias WSResponse = EntryListResult
    
    var offset = 0
    var limit = 50
    var category = 1
    var deviceId = ""
    
    init(offsetNum:Int, limitNum:Int, categoryId:Int, deviceIdString:String) {
        offset = offsetNum
        limit = limitNum
        category = categoryId
        deviceId = deviceIdString
    }
    
    var method: Method {
        return .GET
    }
    
    var path: String {
        return "/ReadFeedData.php"
    }
    
    var parameters: Parameters {
        let params:[String:AnyObject] = ["offset":offset, "limit":limit, "category":category, "device_id":deviceId]
        return params
    }
    
    var encoding: ParameterEncoding {
        return .URL
    }
    
    var URLRequest: NSMutableURLRequest {
        return TCSMWS.URLRequest(method, path: path, parameters: parameters, encoding: encoding)
    }
    
    func responseFromObject(object: NSData, headers: [NSObject:AnyObject]) -> WSResponse? {
        return Mapper<EntryListResult>().map(object)
    }
}

public class EntryListResult: Mappable {
    public var data = [EntryDataResult]()
    
    required public init?(_ map: Map) {}
    
    public func mapping(map: Map) {
        data <- map["data"]
    }
}

public class EntryDataResult: Mappable {
    public var entryTitle: String?
    public var siteName: String?
    public var entryDate: String?
    public var detailUrl: String?
    public var thumbnailUrl: String?
    public var entryId: String?
    public var userId: String?
    
    public init() {}
    
    required public init?(_ map: Map) {}
    
    public func mapping(map: Map) {
        entryTitle                            <- map["entry_title"]
        siteName                              <- map["site_name"]
        entryDate                             <- map["entry_date"]
        detailUrl                             <- map["detail_url"]
        thumbnailUrl                          <- map["thumbnail_url"]
        entryId                               <- map["entry_id"]
        userId                                <- map["user_id"]
    }
}