//
//  OfficialVersion.swift
//  Apple
//
//  Created by 神﨑 翔一朗 on 2016/04/05.
//  Copyright © 2016年 Bizreach. All rights reserved.
//

import Foundation

extension AppleWS.Endpoint {
    typealias OfficialVersion = OfficialVersion$
}

class OfficialVersion$: WSRequest {
    
    typealias WSResponse = OfficialVersionResult
    
    var method: Method {
        return .GET
    }
    
    var path: String {
        return "/lookup"
    }
    
    var parameters: Parameters {
        //Todo: 使う前に自分のアプリID入れること
        return ["id" : "*****"]
    }
    
    var encoding: ParameterEncoding {
        return .URL
    }
    
    var URLRequest: NSMutableURLRequest {
        return AppleWS.URLRequest(method, path: path, parameters: parameters, encoding: encoding)
    }
    
    func responseFromObject(object: NSData, headers: [NSObject:AnyObject]) -> WSResponse? {
        return Mapper<OfficialVersionResult>().map(object)
    }
}


class OfficialVersionResult: TCSMWSResult {
    internal var results = [OfficialVersionItem]()
    
    internal var version: String {
        return results.first?.version ?? "1.0.0"
    }
    
    override internal func mapping(map :Map){
        super.mapping(map)
        results <- map["results"]
    }
}

public class OfficialVersionItem: Mappable {
    public var version = ""
    
    required public init?(_ map: Map) {}
    
    public func mapping(map: Map) {
        version <- map["version"]
    }
}