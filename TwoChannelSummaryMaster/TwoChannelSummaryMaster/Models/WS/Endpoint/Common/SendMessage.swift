//
//  SendMessage.swift
//  TwoChannelSummaryMaster
//
//  Created by 神﨑 翔一朗 on 2016/08/11.
//  Copyright © 2016年 PerriesFactory. All rights reserved.
//

import Foundation

extension TCSMWS.Endpoint {
    typealias SendMessage = SendMessage$
}

class SendMessage$: WSRequest {
    
    typealias WSResponse = SendMessageResult
    
    var deviceId = ""
    var mail = ""
    var message = ""
    
    init(deviceId:String, mail:String, message:String) {
        self.deviceId = deviceId
        self.mail = mail
        self.message = message
    }
    
    var method: Method {
        return .POST
    }
    
    var path: String {
        return "/SendMessage.php"
    }
    
    var parameters: Parameters {
        let params = ["device_id":self.deviceId, "mail_address":self.mail, "message":message]
        return params
    }
    
    var encoding: ParameterEncoding {
        return .JSON
    }
    
    var URLRequest: NSMutableURLRequest {
        let request = TCSMWS.URLRequest(method, path: path, parameters: parameters, encoding: encoding)
        return request
    }
    
    func responseFromObject(object: NSData, headers: [NSObject:AnyObject]) -> WSResponse? {
        return Mapper<SendMessageResult>().map(object)
    }
}

public class SendMessageResult: Mappable {
    public var result:String?
    
    required public init?(_ map: Map) {}
    
    public func mapping(map: Map) {
        result <- map["result"]
    }
}