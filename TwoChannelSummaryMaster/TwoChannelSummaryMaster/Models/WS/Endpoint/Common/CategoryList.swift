//
//  CategoryList.swift
//  TwoChannelSummaryMaster
//
//  Created by 神﨑 翔一朗 on 2016/08/09.
//  Copyright © 2016年 PerriesFactory. All rights reserved.
//

import Foundation

extension TCSMWS.Endpoint {
    typealias CategoryList = CategoryList$
}

class CategoryList$: WSRequest {
    
    typealias WSResponse = CategoryListResult
    
    init() {
    }
    
    var method: Method {
        return .GET
    }
    
    var path: String {
        return "/GetCategoryList.php"
    }
    
    var parameters: Parameters {
        let params = ["":""]
        return params
    }
    
    var encoding: ParameterEncoding {
        return .URL
    }
    
    var URLRequest: NSMutableURLRequest {
        return TCSMWS.URLRequest(method, path: path, parameters: parameters, encoding: encoding)
    }
    
    func responseFromObject(object: NSData, headers: [NSObject:AnyObject]) -> WSResponse? {
        return Mapper<CategoryListResult>().map(object)
    }
}

public class CategoryListResult: Mappable {
    public var data = [CategoryDataResult]()
    
    required public init?(_ map: Map) {}
    
    public func mapping(map: Map) {
        data <- map["data"]
    }
}

public class CategoryDataResult: Mappable {
    public var categoryId: String?
    public var categoryName: String?
    
    public init() {}
    
    required public init?(_ map: Map) {}
    
    public func mapping(map: Map) {
        categoryId                            <- map["category_id"]
        categoryName                          <- map["category_name"]
    }
}