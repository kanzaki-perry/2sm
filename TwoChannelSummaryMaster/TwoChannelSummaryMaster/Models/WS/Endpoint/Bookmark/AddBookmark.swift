//
//  AddBookmark.swift
//  TwoChannelSummaryMaster
//
//  Created by 神﨑 翔一朗 on 2016/08/10.
//  Copyright © 2016年 PerriesFactory. All rights reserved.
//

import Foundation

extension TCSMWS.Endpoint {
    typealias AddBookmark = AddBookmark$
}

class AddBookmark$: WSRequest {
    
    typealias WSResponse = AddBookmarkResult
    
    var deviceId = ""
    var entryId = ""
    
    init(deviceId:String, entryId:String) {
        self.deviceId = deviceId
        self.entryId = entryId
    }
    
    var method: Method {
        return .POST
    }
    
    var path: String {
        return "/BookMarkAdd.php"
    }
    
    var parameters: Parameters {
        let params: [String : AnyObject] = ["device_id":deviceId, "entry_id":entryId]
        return params
    }
    
    var encoding: ParameterEncoding {
        return .JSON
    }
    
    var URLRequest: NSMutableURLRequest {
        let request = TCSMWS.URLRequest(method, path: path, parameters: parameters, encoding: encoding)
        return request
    }
    
    func responseFromObject(object: NSData, headers: [NSObject:AnyObject]) -> WSResponse? {
        return Mapper<AddBookmarkResult>().map(object)
    }
}

class AddBookmarkResult: TCSMWSResult {
    required init?(_ map: Map) {
        super.init(map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map)
    }
}