//
//  DeleteBookmark.swift
//  TwoChannelSummaryMaster
//
//  Created by 神﨑 翔一朗 on 2016/08/10.
//  Copyright © 2016年 PerriesFactory. All rights reserved.
//

import Foundation

extension TCSMWS.Endpoint {
    typealias DeleteBookmark = DeleteBookmark$
}

class DeleteBookmark$: WSRequest {
    
    typealias WSResponse = DeleteBookmarkResult
    
    var deviceId = ""
    var entryId = ""
    
    init(deviceId:String, entryId:String) {
        self.deviceId = deviceId
        self.entryId = entryId
    }
    
    var method: Method {
        return .POST
    }
    
    var path: String {
        return "/BookMarkDelete.php"
    }
    
    var parameters: Parameters {
        let params: [String : AnyObject] = ["device_id":deviceId, "entry_id":entryId]
        return params
    }
    
    var encoding: ParameterEncoding {
        return .JSON
    }
    
    var URLRequest: NSMutableURLRequest {
        let request = TCSMWS.URLRequest(method, path: path, parameters: parameters, encoding: encoding)
        return request
    }
    
    func responseFromObject(object: NSData, headers: [NSObject:AnyObject]) -> WSResponse? {
        return Mapper<DeleteBookmarkResult>().map(object)
    }
}

class DeleteBookmarkResult: TCSMWSResult {
    required init?(_ map: Map) {
        super.init(map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map)
    }
}