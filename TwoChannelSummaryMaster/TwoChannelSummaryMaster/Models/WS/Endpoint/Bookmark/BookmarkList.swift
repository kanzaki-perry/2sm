//
//  BookmarkList.swift
//  TwoChannelSummaryMaster
//
//  Created by 神﨑 翔一朗 on 2016/08/10.
//  Copyright © 2016年 PerriesFactory. All rights reserved.
//

import Foundation

extension TCSMWS.Endpoint {
    typealias BookmarkList = BookmarkList$
}

class BookmarkList$: WSRequest {
    
    typealias WSResponse = BookmarkListResult
    
    var offset = 0
    var limit = 50
    var deviceId = ""
    
    init(offsetNum:Int, limitNum:Int, deviceIdString:String) {
        offset = offsetNum
        limit = limitNum
        deviceId = deviceIdString
    }
    
    var method: Method {
        return .GET
    }
    
    var path: String {
        return "/BookMarkList.php"
    }
    
    var parameters: Parameters {
        let params:[String:AnyObject] = ["offset":offset, "limit":limit, "device_id":deviceId]
        return params
    }
    
    var encoding: ParameterEncoding {
        return .URL
    }
    
    var URLRequest: NSMutableURLRequest {
        return TCSMWS.URLRequest(method, path: path, parameters: parameters, encoding: encoding)
    }
    
    func responseFromObject(object: NSData, headers: [NSObject:AnyObject]) -> WSResponse? {
        return Mapper<BookmarkListResult>().map(object)
    }
}

public class BookmarkListResult: Mappable {
    public var data = [BookmarkDataResult]()
    
    required public init?(_ map: Map) {}
    
    public func mapping(map: Map) {
        data <- map["data"]
    }
}

public class BookmarkDataResult: Mappable {
    public var entryTitle: String?
    public var siteName: String?
    public var entryDate: String?
    public var detailUrl: String?
    public var thumbnailUrl: String?
    public var entryId: String?
    public var userId: String?
    
    public init() {}
    
    required public init?(_ map: Map) {}
    
    public func mapping(map: Map) {
        entryTitle                            <- map["entry_title"]
        siteName                              <- map["site_name"]
        entryDate                             <- map["et_date"]
        detailUrl                             <- map["detail_url"]
        thumbnailUrl                          <- map["thumbnail_url"]
        entryId                               <- map["entry_id"]
        userId                                <- map["user_id"]
    }
}