//
//  UserAdd.swift
//  TwoChannelSummaryMaster
//
//  Created by 神﨑 翔一朗 on 2016/07/14.
//  Copyright © 2016年 PerriesFactory. All rights reserved.
//

import Foundation

extension TCSMWS.Endpoint {
    typealias AddUser = AddUser$
}

class AddUser$: WSRequest {
    
    typealias WSResponse = AddUserResult
    
    var deviceId = ""
    
    init(deviceId:String) {
        self.deviceId = deviceId
    }
    
    var method: Method {
        return .POST
    }
    
    var path: String {
        return "/UserAdd.php"
    }
    
    var parameters: Parameters {
        let params = ["device_id":self.deviceId]
        return params
    }
    
    var encoding: ParameterEncoding {
        return .JSON
    }
    
    var URLRequest: NSMutableURLRequest {
        let request = TCSMWS.URLRequest(method, path: path, parameters: parameters, encoding: encoding)
        return request
    }
    
    func responseFromObject(object: NSData, headers: [NSObject:AnyObject]) -> WSResponse? {
        return Mapper<AddUserResult>().map(object)
    }
}

public class AddUserResult: Mappable {
    public var result:String?
    
    required public init?(_ map: Map) {}
    
    public func mapping(map: Map) {
        result <- map["result"]
    }
}