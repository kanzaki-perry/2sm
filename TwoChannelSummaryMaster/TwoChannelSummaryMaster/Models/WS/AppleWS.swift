//
//  AppleWS.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import Foundation

class AppleWS: WS {
    
    override class func baseURL() -> NSURL {
        return NSURL(string: "https://itunes.apple.com")!
    }
    
    class Endpoint {
        // 各APIエンドポイントの定義は、各APIリクエストクラスで定義すること！
    }
}