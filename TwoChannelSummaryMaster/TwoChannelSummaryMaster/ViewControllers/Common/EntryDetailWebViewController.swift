//
//  EntryDetailWebViewController.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import UIKit

class EntryDetailWebViewController:UIViewController, UIWebViewDelegate{
    
    @IBOutlet weak var webView: UIWebView!
    
    var entryId:String = ""
    var entryURL:String = ""
    var viewURL:String = Environment.URL.WEB + "/api/ViewFilterHtml.php"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        loadViewURL()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = true
    }
    
    func loadViewURL(){
        let requestURL = NSURL(string: viewURL + "?entry=" + entryId)
        let request = NSURLRequest(URL: requestURL!)
        webView.loadRequest(request)
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if (navigationType == UIWebViewNavigationType.LinkClicked) {
            let pattern = entryURL
            let url = request.URL?.description ?? entryURL
            
            if (url.hasPrefix(pattern)) {
                let paramOption = url.stringByReplacingOccurrencesOfString(entryURL, withString: "")
                let encodeParamOption = paramOption.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.alphanumericCharacterSet())!
                let requestURL = NSURL(string: viewURL + "?entry=" + entryId + "&param=" + encodeParamOption)
                let linkRequest = NSURLRequest(URL: requestURL!)
                webView.loadRequest(linkRequest)
            }else{
                let url = request.URL
                if UIApplication.sharedApplication().canOpenURL(url!){
                    UIApplication.sharedApplication().openURL(url!)
                }
            }
            return false
        }
        return true
    }
}
