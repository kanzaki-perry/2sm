//
//  EntryListModel.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import UIKit

class EntryListModel:NSObject{
    var entryTitleString:String
    var entrySiteNameString:String
    var entryDateString:String
    var entryURLString:String
    var entryThumbnailURLString:String
    var entryIdString:String
    var isBookmarkBool:Bool
    
    init(entryTitle:String, entrySiteName:String, entryDate:String, entryURL:String, entryThumbnailURL:String, entryId:String, isBookmark:Bool){
        entryTitleString = entryTitle
        entrySiteNameString = entrySiteName
        entryDateString = entryDate
        entryURLString = entryURL
        entryThumbnailURLString = entryThumbnailURL
        entryIdString = entryId
        isBookmarkBool = isBookmark
    }
}