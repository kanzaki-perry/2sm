//
//  EntryListCell.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import UIKit
import SVProgressHUD

class EntryListCell:UITableViewCell{
    @IBOutlet weak var entryTitle: UILabel!
    @IBOutlet weak var entrySiteName: UILabel!
    @IBOutlet weak var entryDate: UILabel!
    @IBOutlet weak var entryImage: UIImageView!
    @IBOutlet weak var bookmarkButton: UIButton!
    
    var entryData:EntryListModel?
    
    @IBAction func onTapBookmark(sender: UIButton) {
        entryData!.isBookmarkBool ? deleteBookmark() : registBookmark()
        entryData?.isBookmarkBool = !entryData!.isBookmarkBool
        self.bookmarkButton.layer.borderColor = UIColor.blackColor().colorWithAlphaComponent(0.75).CGColor
        self.bookmarkButton.backgroundColor = entryData!.isBookmarkBool ? UIColor.orangeColor().colorWithAlphaComponent(0.25) : UIColor.greenColor().colorWithAlphaComponent(0.25)
        self.bookmarkButton.setTitle(entryData!.isBookmarkBool ? "登録済み" : "あとで読む", forState: .Normal)
    }
    
    func registBookmark(){
        let appId = Configration.sharedInstance.deviceId ?? ""
        let encodeId = appId.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.alphanumericCharacterSet()) ?? ""
        SVProgressHUD.showWithMaskType(.Gradient)
        let request = TCSMWS.Endpoint.AddBookmark(deviceId: encodeId, entryId: self.entryData?.entryIdString ?? "")
        let future = TCSMWS.invoke(request)
        
        future.onSuccess { res in
            SVProgressHUD.dismiss()
        }
        
        future.onFailure { err in
            SVProgressHUD.dismiss()
        }
    }
    
    func deleteBookmark(){
        let appId = Configration.sharedInstance.deviceId ?? ""
        let encodeId = appId.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.alphanumericCharacterSet()) ?? ""
        SVProgressHUD.showWithMaskType(.Gradient)
        let request = TCSMWS.Endpoint.DeleteBookmark(deviceId: encodeId, entryId: self.entryData?.entryIdString ?? "")
        let future = TCSMWS.invoke(request)
        
        future.onSuccess { res in
            SVProgressHUD.dismiss()
        }
        
        future.onFailure { err in
            SVProgressHUD.dismiss()
        }
    }
 
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.layer.cornerRadius = 8
        contentView.layer.borderWidth = 1
        contentView.layer.borderColor = UIColor.blackColor().colorWithAlphaComponent(0.75).CGColor
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = CGRectInset(contentView.bounds, 8, 4)
    }
}