//
//  RootTabBarController.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import UIKit
import TabPageViewController
import RAMAnimatedTabBarController

class RootTabBarController:RAMAnimatedTabBarController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.barTintColor = UIColor.blackColor()
    }
}