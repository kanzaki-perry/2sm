//
//  AdminMessageViewController.swift
//  TwoChannelSummaryMaster
//
//  Created by 神﨑 翔一朗 on 2016/08/11.
//  Copyright © 2016年 PerriesFactory. All rights reserved.
//

import UIKit

class AdminMessageViewController: UIViewController{
    
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var mailTextField: UITextField!
    
    @IBOutlet weak var sendButton: UIButton!
    
    @IBAction func onClickSendButton(sender: UIButton) {
        let count = self.messageTextView.text.characters.count
        if(count < 1 || count > 2000){
            let alert = UIAlertController(title: "送信できません", message: "本文は2000文字以内で入力してください", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "ＯＫ", style: .Cancel, handler: { (action) in
                
            }))
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "メッセージを送信します", message: "いただいたメッセージは今後の機能改善に活用させていただきます。送信してよろしいですか？", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "ＯＫ", style: .Default, handler: { (action) in
                self.sendMessage()
            }))
            alert.addAction(UIAlertAction(title: "キャンセル", style: .Cancel, handler: { (action) in
            }))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.title = "機能要望・不具合報告"
        super.viewWillAppear(animated)
        sendButton.layer.borderWidth = 1
        sendButton.layer.borderColor = UIColor.blackColor().colorWithAlphaComponent(0.75).CGColor
        sendButton.layer.cornerRadius = 5
    }
    
    //非同期JSON通信でAPIから一覧データ取得
    func sendMessage() {
        let appId = Configration.sharedInstance.deviceId ?? ""
        let encodeId = appId.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.alphanumericCharacterSet()) ?? ""
        let request = TCSMWS.Endpoint.SendMessage(deviceId: encodeId, mail: self.mailTextField.text! , message: self.messageTextView.text)
        let future = TCSMWS.invoke(request)
        
        future.onSuccess { res in
            let alert = UIAlertController(title: "送信が完了しました", message: "御意見ありがとうございます。", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "ＯＫ", style: .Cancel, handler: { (action) in
                self.navigationController?.popViewControllerAnimated(true)
            }))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        future.onFailure { err in
            
        }
    }
}