//
//  ConfigViewController.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import UIKit

class ConfigTableViewController:UITableViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "設定"
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.row == 0){
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("AdminMessageViewController") as! AdminMessageViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            
        }
    }
}