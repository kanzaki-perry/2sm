//
//  TopViewController.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import UIKit
import SVProgressHUD
import TabPageViewController
import SDWebImage
import HidingNavigationBar

class ContentsListViewController:UIViewController, NSXMLParserDelegate{
    
    @IBOutlet weak var entryListTableView: UITableView!
    
    var feedList = [EntryListModel]()
    var count:Int = 0
    var offset = 0
    var limit = 50
    var category = 1
    var isMax = false
    var refreshControl = UIRefreshControl()
    var hidingNavBarManager: HidingNavigationBarManager?
    
    @IBAction func onTapReloadButton(sender: UIBarButtonItem) {
        refresh()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hidingNavBarManager = HidingNavigationBarManager(viewController: self, scrollView: entryListTableView)
        
        self.navigationItem.title = "新着記事"
        entryListTableView.dataSource = self
        entryListTableView.delegate = self
        
        let navigationHeight = navigationController?.navigationBar.frame.maxY ?? 0.0
        entryListTableView.contentInset.top = navigationHeight + TabPageOption().tabHeight
        
        // 引っ張ってロードする
        refreshControl.attributedTitle = NSAttributedString(string: "リロード中")
        refreshControl.addTarget(self, action: #selector(ContentsListViewController.refresh), forControlEvents: UIControlEvents.ValueChanged)
        self.entryListTableView.addSubview(refreshControl)
        
        getFeeds()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        hidingNavBarManager?.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        hidingNavBarManager?.viewDidLayoutSubviews()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        hidingNavBarManager?.viewWillDisappear(animated)
    }
    
    //非同期JSON通信でAPIから一覧データ取得
    func getFeeds(isReload:Bool = false) {
        let deviceId = Configration.sharedInstance.deviceId ?? ""
        let encodeId = deviceId.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.alphanumericCharacterSet()) ?? ""
        let request = TCSMWS.Endpoint.EntryList(offsetNum: isReload ? 0 : offset, limitNum: limit, categoryId: category, deviceIdString: encodeId)
        let future = TCSMWS.invoke(request)
        
        SVProgressHUD.showWithMaskType(.Gradient)
        
        future.onSuccess { res in
            
            let count = res.data.count
            self.offset += count
            
            if(count == 0 || count < self.limit){
                self.isMax = true
            }
            
            for entry in res.data{
                self.feedList.append(
                    EntryListModel(entryTitle: entry.entryTitle ?? "", entrySiteName: entry.siteName ?? "", entryDate: entry.entryDate ?? "", entryURL: entry.detailUrl ?? "", entryThumbnailURL: entry.thumbnailUrl ?? "", entryId: entry.entryId ?? "", isBookmark: (entry.userId != nil))
                )
            }
            
            self.entryListTableView.reloadData()
            NSOperationQueue.mainQueue().addOperationWithBlock({
                SVProgressHUD.dismiss()
                if(isReload){
                    self.refreshControl.endRefreshing()
                }
            })
        }
        
        future.onFailure { err in
            SVProgressHUD.dismiss()
        }
    }
    
    func dispatch_async_main(block: () -> ()) {
        dispatch_async(dispatch_get_main_queue(), block)
    }
    
    func dispatch_async_global(block: () -> ()) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block)
    }
    
    func refresh() {
        getFeeds(true)
    }
    
    func readNext(){
        if(!self.isMax){
            getFeeds()
        }
    }
}

extension ContentsListViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedList.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let detailView:EntryDetailWebViewController = self.storyboard?.instantiateViewControllerWithIdentifier("EntryDetailWebView") as! EntryDetailWebViewController
        detailView.entryId = feedList[indexPath.row].entryIdString
        detailView.entryURL = feedList[indexPath.row].entryURLString
        self.navigationController?.pushViewController(detailView
            , animated: true)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:EntryListCell = tableView.dequeueReusableCellWithIdentifier("EntryListCell", forIndexPath: indexPath) as! EntryListCell
        let feedData = feedList[indexPath.row]
        cell.bookmarkButton.layer.borderWidth = 1.0
        cell.bookmarkButton.layer.borderColor = UIColor.blackColor().colorWithAlphaComponent(0.75).CGColor
        cell.bookmarkButton.layer.backgroundColor = feedData.isBookmarkBool ? UIColor.orangeColor().colorWithAlphaComponent(0.25).CGColor : UIColor.greenColor().colorWithAlphaComponent(0.25).CGColor
        cell.bookmarkButton.setTitleColor(UIColor.blackColor().colorWithAlphaComponent(0.75), forState: .Normal)
        cell.bookmarkButton.setTitle(feedData.isBookmarkBool ? "登録済み" : "あとで読む", forState: .Normal)
        cell.entryTitle.text = feedData.entryTitleString
        cell.entrySiteName.text = feedData.entrySiteNameString
        cell.entryDate.text = feedData.entryDateString
        cell.entryImage.layer.cornerRadius = 5
        cell.entryImage.layer.masksToBounds = true
        cell.entryImage.sd_setImageWithURL(NSURL(string: feedData.entryThumbnailURLString), placeholderImage: UIImage(named: "no_image"))
        cell.entryData = feedData
        
        if(indexPath.row == self.feedList.count-1){
            self.readNext()
        }
        
        return cell
    }
    
    func scrollViewShouldScrollToTop(scrollView: UIScrollView) -> Bool {
        hidingNavBarManager?.shouldScrollToTop()
        return true
    }
}