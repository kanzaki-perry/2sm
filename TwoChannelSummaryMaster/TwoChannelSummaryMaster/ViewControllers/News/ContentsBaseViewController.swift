//
//  ContentsBaseViewController.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import TabPageViewController

class ContentsBaseViewController:UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = true
        getCategories()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //非同期JSON通信でAPIから一覧データ取得
    func getCategories() {
        let request = TCSMWS.Endpoint.CategoryList()
        let future = TCSMWS.invoke(request)
        
        future.onSuccess { res in
            self.createTableView(res.data)
        }
        
        future.onFailure { err in

        }
    }
    
    func createTableView(categories:[CategoryDataResult]){
        let tabPageViewController = TabPageViewController.create()
        var items:[(viewController:UIViewController, title:String)] = []
        
        for categoryData in categories{
            let contentsListViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ContentsListViewController") as! ContentsListViewController
            contentsListViewController.category = Int(categoryData.categoryId!)!
            items.append((contentsListViewController, categoryData.categoryName!))
        }
        tabPageViewController.tabItems = items
        tabPageViewController.option.currentColor = UIColor.yellowColor()
        tabPageViewController.option.tabBackgroundColor = UIColor.blackColor()
        tabPageViewController.option.fontSize = 13
        tabPageViewController.option.tabHeight = 44
        tabPageViewController.option.tabWidth = 140
        tabPageViewController.option.defaultColor = UIColor.whiteColor()
        tabPageViewController.isInfinity = true
        self.navigationController?.viewControllers = [tabPageViewController]
    }
}