//
//  RankingBaseViewController.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import UIKit
import TabPageViewController

class RankingBaseViewController:UIViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        createTableView()
    }
    
    func createTableView(){
        let tabPageViewController = TabPageViewController.create()
        var items:[(viewController:UIViewController, title:String)] = []
        
        let rankingListViewController1 = self.storyboard?.instantiateViewControllerWithIdentifier("RankingListViewController") as! RankingListViewController
        rankingListViewController1.rankingType = 1
        items.append((rankingListViewController1, "デイリー"))
        
        let rankingListViewController2 = self.storyboard?.instantiateViewControllerWithIdentifier("RankingListViewController") as! RankingListViewController
        rankingListViewController2.rankingType = 2
        items.append((rankingListViewController2, "週間"))
        
        let rankingListViewController3 = self.storyboard?.instantiateViewControllerWithIdentifier("RankingListViewController") as! RankingListViewController
        rankingListViewController3.rankingType = 3
        items.append((rankingListViewController3, "月間"))
        
        let rankingListViewController4 = self.storyboard?.instantiateViewControllerWithIdentifier("RankingListViewController") as! RankingListViewController
        rankingListViewController4.rankingType = 4
        items.append((rankingListViewController4, "殿堂"))
        
        tabPageViewController.tabItems = items
        tabPageViewController.option.currentColor = UIColor.yellowColor()
        tabPageViewController.option.tabBackgroundColor = UIColor.blackColor()
        tabPageViewController.option.fontSize = 16
        tabPageViewController.option.tabHeight = 44
        tabPageViewController.option.tabWidth = 100
        tabPageViewController.option.defaultColor = UIColor.whiteColor()
        tabPageViewController.isInfinity = true
        self.navigationController?.viewControllers = [tabPageViewController]
    }
}