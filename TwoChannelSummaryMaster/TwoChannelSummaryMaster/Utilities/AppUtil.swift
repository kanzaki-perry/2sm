//
//  AppUtil.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import UIKit
import BlocksKit

class AppUtil {
    
    //ネットワークエラー時のアラートダイアログ
    class func showNetworkAlert(error: NSError, otherErrorHandler: (() -> Void)?) {
        if NSURLErrorDomain == error.domain && NSURLErrorCancelled != error.code {
            let alertView = UIAlertView.bk_alertViewWithTitle("オフラインのため表示できません") as! UIAlertView
            alertView.bk_addButtonWithTitle("OK", handler: { () -> Void in
            })
            alertView.show()
            
        } else {
            if let f = otherErrorHandler {
                f()
            }
        }
    }
    
    //カラーコードをUIColor型に変換
    static func colorWithHexString (hex:String) -> UIColor {
        let cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).uppercaseString
        
        if ((cString as String).characters.count != 6) {
            return UIColor.grayColor()
        }
        
        let rString = (cString as NSString).substringWithRange(NSRange(location: 0, length: 2))
        let gString = (cString as NSString).substringWithRange(NSRange(location: 2, length: 2))
        let bString = (cString as NSString).substringWithRange(NSRange(location: 4, length: 2))
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        NSScanner(string: rString).scanHexInt(&r)
        NSScanner(string: gString).scanHexInt(&g)
        NSScanner(string: bString).scanHexInt(&b)
        
        return UIColor(
            red: CGFloat(Float(r) / 255.0),
            green: CGFloat(Float(g) / 255.0),
            blue: CGFloat(Float(b) / 255.0),
            alpha: CGFloat(Float(1.0))
        )
    }
    
    //画像リサイズ
    static func cropThumbnailImage(image :UIImage, w:Int, h:Int) ->UIImage
    {
        // リサイズ処理
        
        let origRef    = image.CGImage;
        let origWidth  = Int(CGImageGetWidth(origRef))
        let origHeight = Int(CGImageGetHeight(origRef))
        var resizeWidth:Int = 0, resizeHeight:Int = 0
        
        if (origWidth < origHeight) {
            resizeWidth = w
            resizeHeight = origHeight * resizeWidth / origWidth
        } else {
            resizeHeight = h
            resizeWidth = origWidth * resizeHeight / origHeight
        }
        
        let resizeSize = CGSizeMake(CGFloat(resizeWidth), CGFloat(resizeHeight))
        UIGraphicsBeginImageContext(resizeSize)
        
        image.drawInRect(CGRectMake(0, 0, CGFloat(resizeWidth), CGFloat(resizeHeight)))
        
        let resizeImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        // 切り抜き処理
        
        let cropRect  = CGRectMake(
            CGFloat((resizeWidth - w) / 2),
            CGFloat((resizeHeight - h) / 2),
            CGFloat(w), CGFloat(h))
        let cropRef   = CGImageCreateWithImageInRect(resizeImage.CGImage, cropRect)
        let cropImage = UIImage(CGImage: cropRef!)
        
        return cropImage
    }
    
    //アプリ最新バージョンチェック
    static func checkApplicationVersion(viewController:UIViewController? = nil) {
        let request = AppleWS.Endpoint.OfficialVersion()
        let future = AppleWS.invoke(request)
        
        future.onSuccess { res in
            if (res.version > SystemInfo.App.Version) {
                let alertController = UIAlertController(title: "なんかアプリ名決まったら", message: "新バージョンがリリースされています。\nアプリをバージョンアップしてください。", preferredStyle: .Alert)
                
                alertController.addAction(UIAlertAction(title: "ＯＫ", style: .Default, handler:{
                    (action:UIAlertAction!) -> Void in
                    UIApplication.sharedApplication().openURL(NSURL(string: "http://itunes.apple.com/app/id{ID決まったら}")!)
                })
                )
                let baseViewController = viewController
                baseViewController!.presentViewController(alertController, animated: true, completion: nil)
            }
        }
        
        future.onFailure { err in
        }
    }
    
    //WEBViewに自前UserAgentを負荷
    static func setWebViewAgent(){
        let webView:UIWebView = UIWebView()
        webView.frame = CGRectZero
        let userAgent:String! = webView.stringByEvaluatingJavaScriptFromString("navigator.userAgent")
        //Todo:識別子決まったら適当に入れる
        let userAgentStr = "*****"
        let customUserAgent:String = userAgent.stringByAppendingString(userAgentStr)
        let dic:NSDictionary = ["UserAgent":customUserAgent]
        NSUserDefaults.standardUserDefaults().registerDefaults(dic as! [String : AnyObject])
    }
    
    //端末内保存データを一括削除
    static func signOut(){
        let domain = NSBundle.mainBundle().bundleIdentifier!
        NSUserDefaults().removePersistentDomainForName(domain)
        let storage = NSHTTPCookieStorage.sharedHTTPCookieStorage()
        for cookie : NSHTTPCookie in NSHTTPCookieStorage.sharedHTTPCookieStorage().cookiesForURL(NSURL(string: Environment.URL.WEB)!)! as [NSHTTPCookie] {
            storage.deleteCookie(cookie)
        }
    }
    
    // 最前面のViewControllerを取得
    static func getTopViewController()->UIViewController?{
        var topController = UIApplication.sharedApplication().keyWindow?.rootViewController
        while((topController?.presentedViewController) != nil){
            topController = topController?.presentedViewController
        }
        if(topController is UIAlertController){
            return nil
        }else{
            return topController
        }
    }
    
    //文字列からStoryBoard取得
    static func getStoryBoard(name:String)->UIStoryboard{
        return UIStoryboard(name: name, bundle: nil)
    }
}