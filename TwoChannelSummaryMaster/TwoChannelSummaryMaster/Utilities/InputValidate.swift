//
//  InputValidate.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import Foundation

struct InputValidate{
    
    static func validateZipCode(zipCode: String?) -> Bool {
        // 郵便番号
        guard let zipCode = zipCode else {
            return false
        }
        
        let zipCodeRegex = "[0-9]{7}"
        let zipCodeCheck = NSPredicate(format: "SELF MATCHES %@", zipCodeRegex)
        return zipCodeCheck.evaluateWithObject(zipCode)
    }
    
    static func validatePhoneNumber(phoneNumber: String?) -> Bool {
        // 半角数字11桁
        guard let phoneNumber = phoneNumber else {
            return false
        }
        
        let phoneNumberRegex = "[0-9]{11}"
        let phoneNumberCheck = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
        return phoneNumberCheck.evaluateWithObject(phoneNumber)
    }
    
    static func validateEmailAddress(emailAddress: String?) -> Bool {
        // 半角英数 xxx@xxx.xxx 形式 (桁数指定なし)
        guard let emailAddress = emailAddress else {
            return false
        }
        
        let emailAddressRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailCheck = NSPredicate(format: "SELF MATCHES %@", emailAddressRegex)
        return emailCheck.evaluateWithObject(emailAddress)
    }
    
    static func validatePassword(password: String?) -> Bool {
        // 半角英数8-20桁
        guard let password = password else {
            return false
        }
        
        let passwordRegex = "[A-Za-z0-9-]{8,20}"
        let passwordCheck = NSPredicate(format: "SELF MATCHES %@", passwordRegex)
        return passwordCheck.evaluateWithObject(password)
    }
    
    static func validateVerifyCode(verifyCode: String?) -> Bool {
        // 半角数字4桁
        guard let verifyCode = verifyCode else {
            return false
        }
        
        let verifyCodeRegex = "[0-9]{4}"
        let verifyCodeCheck = NSPredicate(format: "SELF MATCHES %@", verifyCodeRegex)
        return verifyCodeCheck.evaluateWithObject(verifyCode)
    }
    
    static func validateKana(kana: String?) -> Bool {
        // ひらがな
        guard let kana = kana else {
            return false
        }
        
        let kanaRegex = "[ぁ-ん 　]*"
        let kanaCheck = NSPredicate(format: "SELF MATCHES %@", kanaRegex)
        return kanaCheck.evaluateWithObject(kana)
    }
    
    static func validateNumber(num: String?) -> Bool {
        // 数値
        guard let num = num else {
            return false
        }
        
        let numRegex = "[1-9][0-9]*"
        let numCheck = NSPredicate(format: "SELF MATCHES %@", numRegex)
        return numCheck.evaluateWithObject(num)
    }
}