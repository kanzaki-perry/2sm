//
//  AppConst.swift
//  TwoChannelSummaryMaster
//
//  Created by 神崎翔一朗 on 2016/07/13.
//  Copyright (c) 2015年 PerriesFactory. All rights reserved.
//

import Foundation

enum AppConst{
    static let KEEP_ENTRIES_KEY = "keepEntryKey"

    /*
    static let FEED_TARGET_LIST:[String] = [
        //アルファルファモザイク
        "http://alfalfalfa.com/index.rdf",
        
        //ゴールデンタイムズ
        "http://blog.livedoor.jp/goldennews/index.rdf",
        //VIPPERな俺
        "http://vippers.jp/index.rdf",
        //痛いニュース
        "http://blog.livedoor.jp/dqnplus/index.rdf",
        //暇人速報
        "http://himasoku.com/index.rdf",
        //ハム速
        "http://hamusoku.com/index.rdf",
        //ニュー速VIPクオリティ
        "http://blog.livedoor.jp/news23vip/index.rdf",
        //ねたAtoZ
        "http://netaatoz.jp/index.rdf",
        //はちまきこう
        "http://blog.esuteru.com/index.rdf",
        //俺的ニュース速報刃
        "http://jin115.com/index.rdf",
        //哲学ニュース
        "http://blog.livedoor.jp/nwknews/index.rdf",
        //ニュー速クオリティ
        "http://news4vip.livedoor.biz/index.rdf",
        //ブル速
        "http://burusoku-vip.com/index.rdf",
        //まじきち速報
        "http://majikichi.com/index.rdf",
        //ブラブラブラウジング
        "http://brow2ing.doorblog.jp/index.rdf",
        //まとめたニュース
        "http://matometanews.com/index.rdf",
        //ニコニコVIP
        "http://blog.livedoor.jp/nicovip2ch/index.rdf",
        //れでぃれでぃ
        "http://blog.livedoor.jp/ladymatome/index.rdf",
        //2chコピペ保存道場
        "http://2chcopipe.com/index.rdf",
        //あやしぃ
        "http://ayacnews2nd.com/index.rdf",
        //はれぞう
        "http://blog.livedoor.jp/darkm/index.rdf",
        //あじゃじゃしたー
        "http://blog.livedoor.jp/chihhylove/index.rdf",
        //エンタメちゃんねる
        "http://blog.livedoor.jp/ringotomomin/index.rdf",
        //時はきたそれだけだ
        "http://tokihakita.blog91.fc2.com/?xml",
        //ワラノート
        "http://waranote.livedoor.biz/index.rdf",
        //咳をしてもゆとり
        "http://yutori2ch.blog67.fc2.com/?xml",
        //ライフハックch
        "http://lifehack2ch.livedoor.biz/index.rdf",
        //無題のドキュメント
        "http://www.mudainodocument.com/index.rdf",
        //かな速
        "http://kanasoku.info/index.rdf",
        //コピペ情報局
        "http://news.2chblog.jp/index.rdf",
        //わろた日記
        "http://blog.livedoor.jp/hisabisaniwarota/index.rdf",
        //妹はVIPPER
        "http://vipsister23.com/index.rdf",
        //ほんわか２ちゃんねる
        "http://honwaka2ch.livedoor.biz/index.rdf",
        //もみあげチャーシュー
        "http://michaelsan.livedoor.biz/index.rdf",
        //ニュー速VIPコレクション
        "http://2chmokomokocat.blog72.fc2.com/?xml",
        //がぞ〜速報
        "http://stalker.livedoor.biz/index.rdf",
        //超漫画速報
        "http://chomanga.com/index.rdf",
        //ONEPEACE速報
        "http://onesoku.com/index.rdf",
        //アンダーワールド
        "http://underworld2ch.blog29.fc2.com/?xml",
        //マニュ速
        "http://2channelmatomenews.blog.fc2.com/?xml",
        //ネトウヨにゅーす
        "http://netouyonews.net/index.rdf",
        //にとそく
        "http://nitosokusinn.blog.fc2.com/?xml",
        //働くものニュース
        "http://workingnews.blog117.fc2.com/?xml",
        //ネギ速
        "http://www.negisoku.com/index.rdf",
        //活目ブログ
        "http://katsumoku.net/index.rdf",
        //ねとそく
        "http://netsoku.net/index.rdf",
        //腹痛い速報まとめ
        "http://haraitai.com/index.rdf",
        //調理兵はVIPPERだった
        "http://blog.livedoor.jp/nonvip/index.rdf",
        //ひまログ
        "http://hima-log.com/index.rdf",
        //V系まとめ
        "http://visual-matome.blog.jp/index.rdf",
        //ゴシップ速報
        "http://gossip1.net/index.rdf",
        //まとめでぃあ
        "http://totalmatomedia.blog.fc2.com/?xml",
        //ラビット速報
        "http://rabitsokuhou.2chblog.jp/index.rdf",
        //DQNニュース
        "http://dqnnews.blog.jp/index.rdf",
        //まとめたろう
        "http://matome-tarou.ldblog.jp/index.rdf",
        
        //IT速報
        "http://blog.livedoor.jp/itsoku/index.rdf",
        //U-1速報
        "http://u1sokuhou.ldblog.jp/index.rdf",
        //BIPブログ
        "http://bipblog.com/index.rdf",
        //政経ちゃんねる
        "http://fxya.blog129.fc2.com/?xml",
        //カイカイ反応通信
        "http://blog.livedoor.jp/kaikaihanno/index.rdf",
        //マネーニュース2ch
        "http://okanehadaiji.com/index.rdf",
        //市況株全力2階建て
        "http://kabumatome.doorblog.jp/index.rdf",
        //株ログNEWS
        "http://kabu-news.blog.jp/index.rdf",
        //いろんなニュース
        "http://iron-na-news.doorblog.jp/index.rdf",
        //2chまとめいっぷく亭
        "http://rssblog.ameba.jp/yumemidri-room/rss20.xml",
        //インターネッツかわら版
        "http://white0wine.blog10.fc2.com/?xml",
        //FX２ちゃんねる
        "http://www.fx2ch.net/index.rdf",
        //経済速報
        "http://businessopen.blog.jp/index.rdf",
        //
        //"",
        //
        //"",
        
        
        
        //なんJPRIDE
        "http://stalker.livedoor.biz/index.rdf",
        //日刊やきう速報
        "http://blog.livedoor.jp/yakiusoku/index.rdf",
        //なんJまとめてはいかんのか？
        "http://blog.livedoor.jp/livejupiter2/index.rdf",
        //なんJスタジアム
        "http://blog.livedoor.jp/nanjstu/index.rdf",
        //ベースボールスレッド
        "http://kyuukaiou.ldblog.jp/index.rdf",

        
        
        //フットボール速報
        "http://football-2ch.com/index.rdf",
        //かるちょまとめブログ
        "http://www.calciomatome.net/index.rdf",
        //SAMURAIGOAL
        "http://samuraigoal.doorblog.jp/index.rdf",
        //SAMURAIFOOTBALLER
        "http://samuraisoccer.doorblog.jp/index.rdf",
        //SAMURAIイレブンまとめ
        "http://samurai-matome.com/feed",
        //フットカルチョ
        "http://blog.livedoor.jp/footcalcio/index.rdf",
        //さからぼ
        "http://sakarabo.blog.jp/index.rdf",
        //サッカー海外の反応
        "http://blog.livedoor.jp/kaigainoomaera-worldsoccer/index.rdf",
        
        
        //NAVERまとめ
        "http://matome.naver.jp/feed/hot",
        
        
        //ねこめも
        "http://nekomemo.com/index.rdf",
        //猫画像まとめ
        "http://nekogazou.net/index.rdf",
        //ねこわん
        "http://nekowan.com/index.rdf",
        //犬・動物ちゃんねる
        "http://animalch.net/index.rdf",
        
        
        //鬼嫁ちゃんねる
        "http://oniyomech.livedoor.biz/index.rdf",
        //いくママ速報
        "http://ikumamasokuhou.com/index.rdf",
        //隣人注意報
        "http://blog.livedoor.jp/rinjinyabai/index.rdf",
        //スカッとしていきませんか
        "http://sk2ch.com/wp-includes/wlwmanifest.xml",
        //はなろぐ
        "http://hanalog.com/index.rdf",
        //子育てちゃんねる
        "http://kosodatech.blog133.fc2.com/?xml",
        //浮気ちゃんねる
        "http://uwakich.com/index.rdf",
        //鬼女速
        "http://kijosoku.com/index.rdf",
        //素敵な鬼女さま
        "http://sutekinakijo.com/index.rdf",
        //きちまま警報
        "http://kichimama.com/index.rdf",
        //既婚者の墓場
        "http://kikonboti.com/index.rdf",
        //本当にやった復習まとめ
        "http://revenge.doorblog.jp/index.rdf",
        //鬼女まとめ速報
        "http://kijyomatome.com/index.rdf",
        //キチママまとめ保管庫
        "http://www.kitimama-matome.net/index.rdf",
        //キスログ
        "http://kisslog2.com/index.rdf",
        //独女ちゃんねる
        "http://dokujyo.net/index.rdf"
    ]
    */
}